import { Component, OnInit } from '@angular/core';
import { Places } from '../models/places'

@Component({
  selector: 'app-place-form',
  templateUrl: './place-form.component.html',
  styleUrls: ['./place-form.component.css']
})
export class PlaceFormComponent implements OnInit {
  // Define the model property within the PlaceFormComponent using the already created Places model as it's type. 
  model: Places = new Places();


  constructor() { }

  ngOnInit() {
  }

  // Add a method named formSubmit() into the PlaceFormComponent. 
  formSubmit() {
    // When this method is called, console.log the information from the model.
    console.log('Submit successful: ', this.model)
  }

}
